# Extend the official Rasa Core SDK image
FROM python:3

# set the working directory in the container to /app
WORKDIR /usr/src/myapp

# copy the dependencies file to the working directory
COPY ./requirements.txt /usr/src/myapp/requirements.txt

# install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# copy the content of the local src directory to the working directory
COPY ./ /usr/src/myapp