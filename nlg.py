import asyncio
# import redis
import random
import ruamel.yaml
from string import Formatter

from sanic import Sanic
from sanic.response import html
from sanic import Blueprint, response, exceptions
from sanic.request import Request
from sanic.response import HTTPResponse
from typing import Optional, Text, Any, List, Dict, Iterable, Callable, Awaitable
from datetime import date

import socketio
import logging
import re
import json


def format_hyperlinks(municipality, response, hyperlinks):
    formatted_response = response
    def repl(matchobj):
        path = matchobj.group(0)[1:-1]
        try:
            name, attribute = path.split(".")
        except ValueError:
            logger.exception(f"Could not split path for {path}")
            return path

        try:
            return hyperlinks[name][municipality][attribute]
        except KeyError as e:
            logger.exception(f"Exception occurred while trying to format hyperlinks: {e}")
            return matchobj.group(0)

    return re.sub(r'<[A-z\.\_]*>', repl, response)


# def get_utter(utter_name, utters, channel, request):
#     default_utters = [u for u in utters if not "channel" in u]
#     usable_utters = default_utters

#     if channel:
#         specific_utters = [u for u in utters if u.get("channel", "") == channel]
#         if specific_utters:
#             usable_utters = specific_utters

#     sender_id = request.json["tracker"]["sender_id"]
#     key = f"{sender_id}-{utter_name}"
#     index = int(r.get(key) or 0)
#     logger.info(f"Retrieving response {key} {index}")

#     utter = usable_utters[index]

#     new_index = 0 if index == len(usable_utters) - 1 else index + 1
#     r.set(key, new_index)
#     return utter


def get_utter_random(utters, channel):
    default_utters = [u for u in utters if not "channel" in u]
    usable_utters = default_utters

    if channel:
        specific_utters = [u for u in utters if u.get("channel", "") == channel]
        if specific_utters:
            usable_utters = specific_utters

    utter = random.choice(usable_utters)

    return utter


logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

# r = redis.Redis(host="redis", port=6379, db=0)

sio = socketio.AsyncServer(async_mode='sanic', cors_allowed_origins=[])
app = Sanic(name="local-nlg")
sio.attach(app,"/socket.io")

yaml = ruamel.yaml.YAML()
responses = yaml.load(open('responses.yml', 'r'))
local_responses = yaml.load(open('local.yml', 'r'))
story_settings = yaml.load(open("story_settings.yml", "r"))
config = yaml.load(open("config.yml", "r"))

def transform_responses_data(responses: Dict):
    """
    Since the local response data is now included in domain.yml, this has
    to be transformed to a more usable data format

    See: https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/2907#note_777524786
    """
    local_responses_data = {}
    for utter_name, utter_data in responses.items():
        if "_lo_" not in utter_name:
            continue

        for entry in utter_data:
            if "custom" in entry:
                local_responses_data[utter_name] = entry["custom"]

    return local_responses_data

local_responses["responses"] = transform_responses_data(responses["responses"])


@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok"})

@app.route("/api/<municipality>/connected", methods=["GET"])
async def municipality_connected(_: Request, municipality: str) -> HTTPResponse:
    connected = municipality in local_responses["municipalities"]
    demo = municipality in (local_responses.get("demo_municipalities") or [])
    return response.json({"connected": connected, "demo": demo})


@app.route("/api/page-metadata", methods=["POST"])
async def livechat_config(request: Request) -> HTTPResponse:
    logger.info("Retrieving livechat config for %s", request.json["organisation"])
    config_data = config.get(request.json["organisation"])
    if not config_data:
        raise exceptions.NotFound("Livechat not found")

    return response.json(config_data["livechat"])


@app.route("/api/<municipality>/stories/<story_id>/", methods=["GET"])
async def story_settings_endpoint(_: Request, municipality: str, story_id: str) -> HTTPResponse:
    enabled = story_settings.get(municipality, {}).get(story_id, {}).get("enabled", True)
    return response.json({"story_id": story_id, "enabled": enabled})


@app.route("/nlg", methods=["POST"])
async def nlg(request: Request) -> HTTPResponse:
    """
    NLG replacement that retrieves the content from the domain file and passes
    the utter name and slots as metadata (used for end to end testing)
    """
    utter_name = request.json["template"]
    slots = request.json["tracker"]["slots"]
    entities = {entity["entity"]: entity["value"] for entity in request.json["tracker"]["latest_message"]["entities"]}
    arguments = request.json["arguments"]
    
    # Extract municipality from widget metadata
    # TODO: Municipality slot zetten aan het begin van het gesprek.
    events = request.json["tracker"]
    user_event_metadata = None
    metadata_municipality = None
    result = [event["metadata"].get("municipality") 
          for event in events.get("events") if event.get("metadata") and not isinstance(event.get("metadata"), list)]
    municipality = result[0].lower()
    if slots.get("municipality", None) == None:
        slots.update({"municipality": result[0]})

    # channel = request.json["tracker"].get("latest_input_channel")
    channel = request.json.get("channel").get("name")
    # FIXME
    # See: https://github.com/RasaHQ/rasa/issues/6432
    # if not channel:
    #     logger.info("`latest_input_channel` not set, falling back to events")
    #     for event in request.json["tracker"]["events"][::-1]:
    #         if event["event"] == "user" and not event.get("metadata", {}).get("is_external", False):
    #             channel = event["input_channel"]
    #             break

    # FIXME hardcoded for now due to channel issues https://gitlab.com/virtuele-gemeente-assistent/gem/-/issues/2369
    if channel == "router":
        channel = "socketio"

    local = False

    logger.info(f"Utter name: {utter_name}")
    logger.info(f"Channel: {channel}")

    if utter_name.startswith("utter_lo_") and municipality in local_responses["municipalities"]:
        logger.info(f'Using local responses file for municipality: {municipality}')
        utter = local_responses["responses"][utter_name]
        hyperlinks = local_responses["hyperlinks"]
        if municipality in utter:
            local = True
            utter = local_responses["responses"][utter_name][municipality][0]
        elif "default" in utter:
            local = True
            logger.info(f"{utter_name} not found in responses for {municipality}, falling back to local responses.yml default")
            utter = local_responses["responses"][utter_name]["default"][0]
        else:
            logger.info(f"{utter_name} not found in responses for {municipality}, falling back to central responses.yml default")
            utter = responses["responses"][utter_name][0]
        central_utter = get_utter_random(responses["responses"][utter_name], channel)
    else:
        logger.info(f'Using central responses file for municipality: {municipality}')
        utter = get_utter_random(responses["responses"][utter_name], channel)
        central_utter = utter

    utter_text = utter.get(channel, utter["text"]) if channel else utter["text"]
    formatted_text = utter_text.format(**{**slots, **arguments})

    if local:
        formatted_text = format_hyperlinks(municipality, formatted_text, hyperlinks)

    format_vars = [fn for _, fn, _, _ in Formatter().parse(utter_text) if fn is not None]
    format_dict = {}
    for var in format_vars:
        format_dict[var] = slots.get(var, arguments.get(var))

    buttons = central_utter.get("buttons", [])
    end2end_buttons = [{"title": b["payload"], "payload": b["payload"]} for b in buttons if b.get("payload", None)]
    custom_data = {"context": {}, "statistics": {}}

    process_dict(central_utter.get("custom", {}).get("context", {}), custom_data["context"], slots, entities, arguments)
    process_dict(central_utter.get("statistics", {}).get("context", {}), custom_data["statistics"], slots, entities, arguments)

    return response.json({
        "text": formatted_text,
        "buttons": buttons,
        "image": central_utter.get("image", None),
        "elements": [],
        "attachments": [],
        # Metadata for end2end tests
        "end2end": {
            "template_name": utter_name,
            "template_vars": format_dict,
            "buttons": end2end_buttons
        },
        "custom": custom_data
    })

def process_dict(data_dict, output_dict, slots, entities, arguments):
    for x, y in data_dict.items():
        if isinstance(y, str):
            try:
                output_dict[x] = y.format(**{**slots, **entities, **arguments})
            except:
                output_dict[x] = y
        else:
            output_dict[x] = y

@sio.event
async def connect(sid, _) -> None:
    logger.info(f"User {sid} connected to socketIO endpoint.")


@sio.event
async def disconnect(sid) -> None:
    logger.info(f"User {sid} disconnected from socketIO endpoint.")


@sio.event
async def session_request(sid: Text, data: Optional[Dict]):
    logger.info(f"User {sid} connected to socketIO endpoint.")
    if data is None:
        data = {}
    if "session_id" not in data or data["session_id"] is None:
        id = 123
        key = "L"
        data["session_id"] = "{:.1}{:04d}/{:%Y%m%d}".format(key,id, date.today())
    await sio.emit("session_confirm", data["session_id"], room=sid)
    logger.info(f"User {sid} connected to socketIO endpoint.")


@sio.event
async def user_uttered(sid, data):
    logger.info(f"User {sid} connected to socketIO endpoint.")
    await sio.emit("bot_uttered", {"text": data["message"]}, room=sid)


if __name__ == '__main__':
    app.run(host="0.0.0.0")